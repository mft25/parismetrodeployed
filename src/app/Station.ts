export class Station {
  public readonly StationId: number;
  public readonly Name: string;
  public readonly ConnectingLines: Array<number>;

  public ImagePath: string;

  public IsExpanded: boolean;

  constructor(stationJson: any) {
    this.StationId = stationJson.StationId;
    this.Name = stationJson.Name;
    this.ConnectingLines = stationJson.ConnectingLines;

    this.ImagePath = null;

    this.IsExpanded = false;
  }

  public ChangeExpansion() {
    this.IsExpanded = !this.IsExpanded;
  }

  public GetConnectingLinesString(): string {
    return this.ConnectingLines.join(', ');
  }
}
