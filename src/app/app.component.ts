import { Component } from "@angular/core";
import { Line } from "./Line";
import { Metro } from "./Metro";
import { Station } from "./Station";
import { LineService } from "./LineService";
import { MetroService } from "./MetroService";
import { StationService } from "./StationService";

@Component({
  selector: "app-metro",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  line: Line;
  metro: Metro;

  constructor(
    private lineService: LineService,
    private metroService: MetroService,
    private stationService: StationService
  ) {
    this.getMetro(1); // 1=Paris
  }

  getMetro(metroId: number) {
    this.metroService.getMetro(metroId).then(() => {
      this.metro = this.metroService.metro;
    });
  }

  getLine(selectedLineId: string) {
    this.metro = null;
    this.lineService.getLine(Number(selectedLineId)).then(() => {
      this.line = this.lineService.line;
    });
  }

  changeStationExpansion(station: Station) {
    station.ChangeExpansion();

    if (station.IsExpanded && station.ImagePath == null) {
      this.stationService.SetImagePath(station);
    }
  }
}
