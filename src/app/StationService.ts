import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Station } from './Station';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class StationService {
  private _getImagePath = function(id) { return `assets/images/stations/${id.toString()}.jpg`; };

  constructor(private http: HttpClient) { }

  public SetImagePath(station: Station) {
    return new Promise((resolve, reject) => {
      const imageUrl = this._getImagePath(station.StationId);

      this.http.get(imageUrl, { responseType: 'blob' })
        .toPromise()
        .then(
          res => {
            station.ImagePath = imageUrl;
          },
          msg => {
            station.ImagePath = this._getImagePath(0);
          }
        );
    });
  }
}
