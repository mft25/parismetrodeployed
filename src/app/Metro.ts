import { Station } from './Station';

export class Metro {
  public readonly MetroId: number;
  public readonly Name: string;
  public readonly Lines: Array<Line>;

  constructor(metroJson: any) {
    this.MetroId = metroJson.MetroId;
    this.Name = metroJson.Name;
    this.Lines = metroJson.Lines.map(lineJson => {
      return new Line(lineJson);
    });
  }
}

export class Line {
  public readonly LineId: number;
  public readonly Name: string;
  public readonly Colour: string;
  public readonly TextColour: string;

  constructor(lineJson: any) {
    this.LineId = lineJson.LineId;
    this.Name = lineJson.Name;
    this.Colour = lineJson.Colour;
    this.TextColour = lineJson.TextColour;
  }
}
