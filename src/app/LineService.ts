import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/toPromise";
import { Line } from "./Line";

@Injectable()
export class LineService {
  metroServiceUrl = "http://localhost:2848";
  getLinePath = "api/metro/lines/getline";

  line: Line;

  constructor(private http: HttpClient) {}

  getLine(lineId: number) {
    let promise = new Promise((resolve, reject) => {
      let apiUrl = `${this.metroServiceUrl}/${this.getLinePath}?LineId=${lineId}&culture=en-gb`;

      this.http
        .get(apiUrl)
        .toPromise()
        .then(
          res => {
            this.line = new Line(res);
            resolve();
          },
          msg => {
            reject();
          }
        );
    });

    return promise;
  }
}
