import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/toPromise";
import { Line, Metro } from "./Metro";

@Injectable()
export class MetroService {
  metroServiceUrl = "http://localhost:2848";
  getMetroPath = "api/metro/metros/getmetro";

  metro: Metro;

  constructor(private http: HttpClient) {}

  getMetro(metroId: number) {
    let promise = new Promise((resolve, reject) => {
      let apiUrl = `${this.metroServiceUrl}/${this.getMetroPath}?MetroId=${metroId}&culture=en-gb`;

      this.http
        .get(apiUrl)
        .toPromise()
        .then(
          res => {
            this.metro = new Metro(res);
            resolve();
          },
          msg => {
            reject();
          }
        );
    });

    return promise;
  }
}
