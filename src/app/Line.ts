import { Station } from './Station';

export class Line {
  public readonly LineId: number;
  public readonly Name: string;
  public readonly Colour: string;
  public readonly TextColour: string;
  public readonly Stations: Array<Station>;

  private ExpandedStationId: number;

  constructor(lineJson: any) {
    this.ExpandedStationId = 0;

    this.LineId = lineJson.LineId;
    this.Name = lineJson.Name;
    this.Colour = lineJson.Colour;
    this.TextColour = lineJson.TextColour;
    this.Stations = lineJson.Stations.map(stationJson => {
      return new Station(stationJson);
    });
  }
}
