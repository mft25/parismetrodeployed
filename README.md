# Metro

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Publish

First build the site in production mode.

Log in to the AWS console at https://eu-west-3.console.aws.amazon.com/console/home?nc2=h_ct&region=eu-west-3&src=header-signin#
Then go to S3 buckets.
Click on metro website, then Upload.
Paste the contents of the dist folder into the bucket.
Next, then grant public read access, Upload.